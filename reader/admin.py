from django.contrib import admin
from reader.models import Author, Book, Page, Section, Image, Text, ImageText

class AuthorAdmin(admin.ModelAdmin):
	list_display = ('last_name', 'first_name', 'email')
	search_fields = ('first_name', 'last_name')

class BookAdmin(admin.ModelAdmin):
	list_display = ('book_title', 'authors')
	search_fields = ('book_title', 'authors')
	ordering = ('-book_title',)
	
admin.site.register(Author, AuthorAdmin)
admin.site.register(Book, BookAdmin)
admin.site.register(Page)
admin.site.register(Section)
admin.site.register(Image)
admin.site.register(Text)
admin.site.register(ImageText)