from django.conf.urls import patterns, include, url
from bookapp.views import *
from django.conf import settings
from django.conf.urls.static import static

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^search/$', search),
    url(r'^upload/$', upload_file),
    url(r'^book/(\d{1,5})/$', read_book),
    url(r'^all/$', book_list),
    url(r'^$', index),
    url(r'^admin/', include(admin.site.urls)),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)