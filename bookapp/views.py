from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.core.context_processors import csrf
from django.core.urlresolvers import reverse
from django.template.loader import get_template
from django.template import Context, RequestContext
from reader.models import *
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, render_to_response
from forms import DocumentForm
import pdb,json,sys
import re

global num
num = 1

def book_list(request):
	books = Book.objects.order_by('book_title')
	global num
	num = 1
	return render(request, 'all.html', {'books': books})

def search_form(request):
	return render(request, 'search_form.html')

def index(request):
	return render(request, 'index.html')
	
def search(request):
	global num
	num = 1
	errors = []
	if 'q' in request.GET:
		q = request.GET['q']
		if not q:
			errors.append("Enter a search term.")
		elif len(q) > 25:
			errors.append("Please enter less than 25 characters.")
		elif not (re.match('^[a-zA-Z0-9-_\s]*$',q)):
			errors.append("Please only enter alphanumeric characters")
		else:
			books = Book.objects.filter(book_title__icontains=q)
			return render(request, 'search_results.html',
				{'books': books, 'query': q})
	return render(request, 'search_form.html', {'errors': errors})

def upload_file(request):
	def __unicode__(self):
		return self.json_data
	params = {}
	new_file = ""
	result = "Please choose a file."
	params.update(csrf(request))
	form = DocumentForm(request.POST, request.FILES)
	if request.method == 'POST':
		if form.is_valid():
			try:
				new_file = json.load(request.FILES['docfile'])
				result = parse_json(new_file)
			except:
				result = "File is not valid JSON."
		return render_to_response('upload_file.html', {"form": form, "new_file": new_file, "result": result}, context_instance=RequestContext(request))
	else:
		form = DocumentForm()
	return render(request, "upload_file.html", {"form": form, "new_file": new_file, "result": result}, context_instance=RequestContext(request))

def parse_json(json_file):
	def __unicode__(self):
		return self.json_file
	try:
		a = Author.objects.get(id=1)
	except:
		a = Author(first_name="Test",last_name="Author")
		a.save()
	try:
		b = Book.objects.get(book_title=json_file["books"][0]["title"])
		errors = "That book has already been added to the database."
		return errors
	except:
		b = Book(book_title=json_file["books"][0]["title"], authors=a)
		b.save()
		page_count = 1
		for i in json_file["books"][0]["pages"]:
			p = Page(book=b, number=page_count)
			p.save()
			page_count = page_count + 1
			section_count = 1
			for k in json_file["books"][0]["pages"][page_count - 2]["sections"]:
				s = Section(page_id=p.id, number=section_count)
				s.save()
				section_count = section_count + 1
				if k["type"] == "image":
					i = Image(section_id=s.id, image=k["image"])
					i.save()
				elif k["type"] == "text":
					i = Text(section_id=s.id, text=k["text"])
					i.save()
				elif k["type"] == "image_text":
					i = ImageText(section_id=s.id, image=k["image"], text=k["text"])
					i.save()
		success = b.book_title + " has been successfully added to the database."
		return success

def read_book(request, book_id):
	try:
		book_id = int(book_id)
	except ValueError:
		raise Http404()
	pages = []
	errors = []
	global num
	book = Book.objects.get(id=book_id)
	p = Page.objects.filter(book_id=book.id)
	for a in p:
		s = Section.objects.filter(page_id=a.id)
		content = []
		for c in s:
			try:
				t = Text.objects.get(section_id=c.id)
				content.append(t.text)
			except:
				pass
			try:
				i = Image.objects.get(section_id=c.id)
				content.append(i.image)
			except:
				pass
			try:
				ti = ImageText.objects.get(section_id=c.id)
				content.append(ti.image)
				content.append(ti.text)
			except:
				pass
		pages.append(content)
	plength = len(pages)
	if 'pagenum' in request.GET:
		try:
			pagenum = int(request.GET['pagenum'])
			if (pagenum > 0) and (pagenum <= plength):
				num = pagenum
			else:
				errors.append("Please enter a valid page number.")
		except:
			errors.append("Please enter a valid page number.")
	prev = num - 1;
	nxt = num + 1;
	t = get_template('book_section.html')
	html = t.render(Context({'book': book, 'pages': pages, 'plength': plength, 'num': num, 'prev': prev, 'nxt': nxt,'errors': errors}))
	return HttpResponse(html)